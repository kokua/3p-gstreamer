#!/bin/bash

# turn on verbose debugging output for teamcity logs.
set -x

# make errors fatal
set -e
TOP="$(dirname "$0")"

PROJECT=gstreamer
BASE_PROJECT=gst-plugins-base
LICENSE=README
GSTREAMER_VERSION="0.10.36"
GSTREAMER_SOURCE_DIR="$PROJECT-$GSTREAMER_VERSION"
PLUGIN_SOURCE_DIR="$BASE_PROJECT-$GSTREAMER_VERSION"

if [ -z "$AUTOBUILD" ] ; then
    echo 'AUTOBUILD not set' 1>&2
    exit 1
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# load autobuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

# This version helps define which archive will be used to extract files from
# Should match the viewer from which the plugins and launcher were extracted from

top="$(pwd)"
stage="$top"

case "$AUTOBUILD_PLATFORM" in
"windows")
    python -c "import zipfile; zipfile.ZipFile('../slvoice-${SLPLUGINS_VERSION}-Release-win32.zip').extractall()"
;;
"darwin")

;;
"linux")
		if [ -d ${stage}/${GSTREAMER_SOURCE_DIR} ]; then
			echo "Removing source because gstreamer needs a fresh tree each build. "
			rm -Rf ${stage}/${GSTREAMER_SOURCE_DIR}
			rm -Rf ${stage}/${PLUGIN_SOURCE_DIR}
			rm -Rf ${stage}/lib
			rm -Rf ${stage}/include
		fi
		tar -xvf ../gstreamer-${GSTREAMER_VERSION}.tar.xz
		tar -xvf ../gst-plugins-base-${GSTREAMER_VERSION}.tar.bz2

        if [[ -x /usr/bin/gcc-4.6 && -x /usr/bin/g++-4.6 ]]; then
            export CC=/usr/bin/gcc-4.6
            export CXX=/usr/bin/g++-4.6
        fi
        export PKG_CONFIG_PATH=${stage}/${GSTREAMER_SOURCE_DIR}/pkgconfig:${stage}/${PLUGIN_SOURCE_DIR}/pkgconfig:${PKG_CONFIG_PATH}
        echo $PKG_CONFIG_PATH
        # Default target to 32-bit
        opts="${TARGET_OPTS:--m32}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi

		cd ${stage}/${GSTREAMER_SOURCE_DIR}
        CFLAGS="$opts -O3 -fPIC -DPIC" CXXFLAGS="$opts -O3 -fPIC -DPIC" \
		sed -i  -e '/YYLEX_PARAM/d' \
				-e '/parse-param.*scanner/i %lex-param { void *scanner }' \
				    gst/parse/grammar.y &&
		./configure --prefix=${stage} \
				    --disable-static &&
		make &&
		make install

		cd ${stage}/${PLUGIN_SOURCE_DIR}
        CFLAGS="$opts -O3 -fPIC -DPIC" CXXFLAGS="$opts -O3 -fPIC -DPIC" \
		./configure --prefix=${stage} &&
		make &&
		make install 
		rsync -av --include '*/' --include '*.h' --exclude '*' gst-libs/gst ${stage}/include/gstreamer-0.10 
		
		mkdir -p "$stage/lib/release"
		cd "$stage/lib"
		cp -a ${stage}/${PLUGIN_SOURCE_DIR}/gst-libs/gst/audio/.libs/libgstaudio-0.10.so* release
		cp -a ${stage}/${PLUGIN_SOURCE_DIR}/gst-libs/gst/video/.libs/libgstvideo-0.10.so* release	
		mv -uf *.so* release
		mv -uf gstreamer-0.10 release
		mv -uf pkgconfig release
		
		
		


;;
"linux64")
		if [ -d ${stage}/${GSTREAMER_SOURCE_DIR} ]; then
			echo "Removing source because gstreamer needs a fresh tree each build. "
			rm -Rf ${stage}/${GSTREAMER_SOURCE_DIR}
			rm -Rf ${stage}/${PLUGIN_SOURCE_DIR}
			rm -Rf ${stage}/lib
			rm -Rf ${stage}/include
		fi
		tar -xvf ../gstreamer-${GSTREAMER_VERSION}.tar.xz
		tar -xvf ../gst-plugins-base-${GSTREAMER_VERSION}.tar.bz2
        if [[ -x /usr/bin/gcc-4.6 && -x /usr/bin/g++-4.6 ]]; then
            export CC=/usr/bin/gcc-4.6
            export CXX=/usr/bin/g++-4.6
        fi
        export PKG_CONFIG_PATH=${stage}/${GSTREAMER_SOURCE_DIR}/pkgconfig:${stage}/${PLUGIN_SOURCE_DIR}/pkgconfig:${PKG_CONFIG_PATH}
        echo $PKG_CONFIG_PATH
        # Default target to 64-bit
        opts="${TARGET_OPTS:--m64}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi

		cd ${stage}/${GSTREAMER_SOURCE_DIR}
        CFLAGS="$opts -O3 -fPIC -DPIC" CXXFLAGS="$opts -O3 -fPIC -DPIC" \
		sed -i  -e '/YYLEX_PARAM/d' \
				-e '/parse-param.*scanner/i %lex-param { void *scanner }' \
				    gst/parse/grammar.y &&
		./configure --prefix=${stage} \
				    --disable-static &&
		make &&
		make install

		cd ${stage}/${PLUGIN_SOURCE_DIR}
        CFLAGS="$opts -O3 -fPIC -DPIC" CXXFLAGS="$opts -O3 -fPIC -DPIC" \
		./configure --prefix=${stage} &&
		make &&
		make install 
		rsync -av --include '*/' --include '*.h' --exclude '*' gst-libs/gst ${stage}/include/gstreamer-0.10 
		
		mkdir -p "$stage/lib/release"
		cd "$stage/lib"
		cp -a ${stage}/${PLUGIN_SOURCE_DIR}/gst-libs/gst/audio/.libs/libgstaudio-0.10.so* release
		cp -a ${stage}/${PLUGIN_SOURCE_DIR}/gst-libs/gst/video/.libs/libgstvideo-0.10.so* release	
		mv -uf *.so* release
		mv -uf gstreamer-0.10 release
		mv -uf pkgconfig release
		

;;

esac
# Create the version number file from variable in this file
version=${GSTREAMER_VERSION}
build=${AUTOBUILD_BUILD_ID:=0}
echo "${version}.${build}" > "${stage}/VERSION.txt"

# Copy over other files
mkdir -p "$stage/LICENSES"
cp "${stage}/${GSTREAMER_SOURCE_DIR}/COPYING" "${stage}/LICENSES/gstreamer.txt"

pass
